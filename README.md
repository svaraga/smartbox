### SmartBox Assessment 

#### Tools used

- JavaScript
- Cypress
-	Page Object Model
-	Cucumber
-	GitHub

#### Pre-requisites
> Node, IntelliJ Idea, Git

Clone or download the project to your local.

- Run ***npm install*** to install the dependencies.
- Url can be configured in file cypress.json
- To execute tests using the cypress test runner, use the command ***npm run test***
- To execute tests on commandline (headless mode), use the command ***npm run cypress:cucumber***
