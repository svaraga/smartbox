import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';
import HomePage from '../pages/HomePage';
import ProductsPage from '../pages/ProductsPage';
import ShoppingCartPage from '../pages/ShoppingCartPage';

/**
*   Step Definition file for @AddToCart.feature
*/

const homePage = new HomePage();
const productsPage = new ProductsPage();
const shoppingCartPage = new ShoppingCartPage();

Cypress.on('uncaught:exception', (err, runnable) => {
  return false
})

 Given('Customer is on the homepage', function () {
    homePage.loadHomePage();
 });

 When('Customer clicks on a random box from the {string} section', function (string) {
    homePage.clickFirstPopularGiftBox()
 });

 When('Customer clicks on the {string} button', function (string) {
    productsPage.clickAddToCart(string)
    productsPage.isItemAddedToCartMsg()
 });

 Then('Box is added to the shopping cart page', function () {
    let cartItem = productsPage.gotoCart()
    shoppingCartPage.validateCartPage(cartItem)
 });