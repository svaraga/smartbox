@cart @smoke
Feature: Add to Cart

  @addtocart
  Scenario: Add a box from 'Our Popular Box' section to cart

    Given Customer is on the homepage
    When Customer clicks on a random box from the 'Our popular box' section
    And Customer clicks on the 'Add to cart' button
    Then Box is added to the shopping cart page