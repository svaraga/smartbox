/**
*   Page Object class for Shopping cart page
*/
class ShoppingCartPage {

    /**
    *   Validates that the cart page is not empty and items are visible
    */
    validateCartPage(cartItemTitle) {
        cy.get('#cart-items .item__box-title').should('be.visible')
    }
}

export default ShoppingCartPage