/**
*   PageObject class for Products page
*/
class ProductsPage {

    clickAddToCart(btnText) {
        cy.get('.add-to-cart__content > span').contains(btnText).click()
    }

    isItemAddedToCartMsg() {
        cy.get('#addtocart-confirmation p').should('contain', 'Item(s) added to your cart')
    }

    gotoCart() {
        cy.get('span.qa-ebox-name').invoke('text').as('text')
        cy.get('a.button').contains('Go to cart').click()
        return this.text
    }

}

export default ProductsPage