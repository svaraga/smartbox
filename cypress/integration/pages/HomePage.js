/**
*   PageObject class for Home page
*/
class HomePage {

    /**
    *   Loads the application. Url is set in the configuration file
    */
    loadHomePage() {
        cy.visit('/')
    }

    /**
    *   Scrolls to the First Popular Gift Box and clicks
    */
    clickFirstPopularGiftBox() {
        cy.get('section[data-productlist="Our most popular gift boxes"]').scrollIntoView()
        cy.get('section[data-productlist="Our most popular gift boxes"] > div div[class*="button"]').contains('See more').click({force: true})
    }
}

export default HomePage